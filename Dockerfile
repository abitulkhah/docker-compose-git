ARG DOCKER_VERSION=latest
FROM docker:${DOCKER_VERSION}

ARG COMPOSE_VERSION=

ENV NO_ROOT_MODE=${NO_ROOT_MODE:-0}

RUN apk add --update --no-cache py-pip python-dev libffi-dev openssl-dev gcc libc-dev make curl openssl git openssh su-exec

RUN pip install --upgrade pip

RUN pip install "docker-compose${COMPOSE_VERSION:+==}${COMPOSE_VERSION}"

RUN docker --version && \
    docker-compose --version 

LABEL \
  org.opencontainers.image.authors="Abitulkhah abimeiiku@gmail.com>" \
  org.opencontainers.image.description="This docker image installs docker-compose on top of the docker image." \
  org.opencontainers.image.licenses="MIT" \
  org.opencontainers.image.title="Docker Compose on docker base image" \
  org.opencontainers.image.vendor="Abitul" \
  org.opencontainers.image.version="${DOCKER_VERSION} with docker-compose ${COMPOSE_VERSION}"

COPY docker-entrypoint.sh /usr/local/bin/

ENTRYPOINT ["docker-entrypoint.sh"]
CMD ["sh"]